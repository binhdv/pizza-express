<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

# Pizza Management
Route::group(array('prefix' => 'pizzas'), function () {
    Route::get('/', 'PizzaController@index');
    Route::post('/', 'PizzaController@store');
    Route::get('{pizza_id}', 'PizzaController@show');
    Route::post('{pizza_id}/update', 'PizzaController@update');
});

# Order Management
Route::group(array('prefix' => 'orders'), function () {
    Route::get('/', 'OrderController@index');
    Route::post('/', 'OrderController@store');
    Route::get('{order_id}', 'OrderController@show');
    Route::post('{order_id}/processing', 'OrderController@processing');
    Route::post('{order_id}/delivering', 'OrderController@delivering');
    Route::post('{order_id}/delivered', 'OrderController@delivered');
});