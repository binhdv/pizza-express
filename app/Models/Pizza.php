<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Pizza extends Model {

    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'pizzas';

    protected $primaryKey = 'pizza_id';

    /**
	 * The attributes to be fillable from the model.
	 *
	 * A dirty hack to allow fields to be fillable by calling empty fillable array
	 *
	 * @var array
	 */
	protected $fillable = ['name','type','description','price'];

}
