<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Order extends Model {

    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'orders';

    protected $primaryKey = 'order_id';

    /**
	 * The attributes to be fillable from the model.
	 *
	 * A dirty hack to allow fields to be fillable by calling empty fillable array
	 *
	 * @var array
	 */
	protected $fillable = ['customer_name','customer_address','customer_phone','customer_description','total','status'];

    public function pizzas(){
        return $this->belongsToMany('App\Models\Pizza','order_pizzas','order_id','pizza_id')->withTimestamps();
    }
}
