<?php 

namespace App\Extensions\Serializers;


use League\Fractal\Pagination\CursorInterface;
use League\Fractal\Pagination\PaginatorInterface;
use League\Fractal\Serializer\ArraySerializer;

class CustomSerializer extends ArraySerializer{

    public function collection($resourceKey, array $data)
    {
        if ($resourceKey === false || is_null($resourceKey)) {
            return $data;
        }
        return array($resourceKey ?: 'data' => $data);
    }

    public function item($resourceKey, array $data)
    {
        if ($resourceKey === false || is_null($resourceKey)) {
            return $data;
        }
        return array($resourceKey ?: 'data' => $data);
    }
}
