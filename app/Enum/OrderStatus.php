<?php
/**
 * Created by PhpStorm.
 * User: macos
 * Date: 6/16/17
 * Time: 12:02 AM
 */

namespace App\Enum;

class OrderStatus
{
    const ORDER_CREATED = 'Created';
    const ORDER_PROCESSING = 'Processing';
    const ORDER_DELIVERING = 'Delivering';
    const ORDER_DELIVERED = 'Delivered';

    public static function getStatus($status) {
        switch ($status) {
            case 0 :
                return self::ORDER_CREATED;
            case 1 :
                return self::ORDER_PROCESSING;
            case 2 :
                return self::ORDER_DELIVERING;
            case 3 :
                return self::ORDER_DELIVERED;
            default :
                return self::ORDER_CREATED;
        }
    }
}

