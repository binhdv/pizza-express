<?php
namespace App\Http\Controllers;

use Illuminate\Http\Response as IlluminateResponse;


class ApiController extends Controller {

	public  $authenticatedUser;
	protected  $translator;

	function __construct(){
		$this->translator = app('translator');
		$this->translator->setLocale(env('APP_LOCALE'));
	}

	/**
	 * @var int
	 */
	protected $statusCode = IlluminateResponse::HTTP_OK;

	/**
	 * @return mixed
	 */
	public function getStatusCode()
	{
		return $this->statusCode;
	}

	/**
	 * @param mixed $statusCode
	 * @return mixed
	 */
	public function setStatusCode($statusCode)
	{
		$this->statusCode = $statusCode;

		return $this;
	}

	/**
	 * @param string $message
	 * @return mixed
	 */
	public function respondNotFound($message = '')
	{
		return $this->setStatusCode(IlluminateResponse::HTTP_NOT_FOUND)->respondWithError($message);
	}

	/**
	 * @param string $message
	 * @return mixed
	 */
	public function respondInternalError($message = '')
	{
		return $this->setStatusCode(IlluminateResponse::HTTP_INTERNAL_SERVER_ERROR)->respondWithError($message);
	}

	/**
	 * @param $message
	 * @return mixed
	 */
	public function respondCreated($message)
	{
		return $this->setStatusCode(IlluminateResponse::HTTP_CREATED)->respond([
			'message' => $message
		]);
	}

	/**
	 * @param $message
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function respondOk($message)
	{
		return $this->respond([
			'message' => $message
		]);
	}

	/**
	 * @param string $message
	 * @return mixed
	 */
	public function respondWithValidationError($message = '')
	{
		return $this->setStatusCode(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY)->respondWithError([
			'message' => $message
		]);
	}

	/**
	 * @param $data
	 * @param array $headers
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function respond($data, $headers = [])
	{
		return response()->json($data, $this->getStatusCode(), $headers);
	}

	/**
	 * @param $message
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function respondWithError($messages)
	{
		$str = $messages;
		if(is_array($messages)){
			$str = '';
			foreach($messages as $message){
				$str .=$message.' ';
			}
		}
		return $this->setStatusCode(IlluminateResponse::HTTP_BAD_REQUEST)->respond([
			'error' => $str
		]);
	}

	public function respondWithCORS($data)
	{
		return $this->respond($data, $this->setCORSHeaders());
	}

	private function setCORSHeaders()
	{
		$header['Access-Control-Allow-Origin'] = '*';
		$header['Allow'] = 'GET, POST, OPTIONS';
		$header['Access-Control-Allow-Headers'] = 'Origin, Content-Type, Accept, Authorization, X-Request-With';
		$header['Access-Control-Allow-Credentials'] = 'true';

		return $header;
	}

}