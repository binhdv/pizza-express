<?php namespace App\Http\Controllers;

use App\Extensions\Serializers\CustomSerializer;
use App\Models\Pizza;
use App\Transformers\PizzaTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class PizzaController extends ApiController
{

    protected $validationPizza = [
		'name'=>'required|min:5',
		'price'=>'required|numeric',
		'type'=>'required|min:2|max:255',
		'description'=>'required|max:255',
	];

    /**
     * Show a list of all the pizzas.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Manager $fractal, PizzaTransformer $pizzaTransformer)
    {
        $pizzas = Pizza::all();
        //
        $fractal->setSerializer(new CustomSerializer());
        $collection = new Collection($pizzas, $pizzaTransformer);
        $data = $fractal->createData($collection)->toArray();
        return $this->respond($data);
    }

    /**
     * Create new pizza.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, Manager $fractal, PizzaTransformer $pizzaTransformer)
    {
        $validator = Validator::make($request->all(), $this->validationPizza);
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return $this->respondWithError($errors);
        }
        //
        $pizza = new Pizza($request->all());
        if ($pizza->save()) {
            $fractal->setSerializer(new CustomSerializer());
            $collection = new Item($pizza, $pizzaTransformer);
            $data = $fractal->createData($collection)->toArray();
            return response()->json($data);
        }
        return $this->respondWithError('Failed to create new pizza');
    }

    /**
     * Get pizza by id.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($pizza_id, Manager $fractal, PizzaTransformer $pizzaTransformer)
    {
        $pizza = Pizza::wherePizza_id($pizza_id)->first();
        if ($pizza != null) {
            $fractal->setSerializer(new CustomSerializer());
            $collection = new Item($pizza, $pizzaTransformer);
            $data = $fractal->createData($collection)->toArray();
            return response()->json($data);
        }
        return $this->respondWithError('Pizza not found');
    }

}
