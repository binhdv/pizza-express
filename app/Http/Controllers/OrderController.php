<?php namespace App\Http\Controllers;

use App\Extensions\Serializers\CustomSerializer;
use App\Models\Order;
use App\Models\Pizza;
use App\Transformers\OrderTransformer;
use App\Transformers\PizzaTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class OrderController extends ApiController
{

    protected $validationOrder = [
		'customer_name'=>'required|min:2',
		'customer_phone'=>'required|numeric|min:5',
		'customer_address'=>'required|min:2|max:255',
		'customer_description'=>'required|max:255',
		'pizzas'=>'required',
	];

    /**
     * Show a list of all the orders.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Manager $fractal, OrderTransformer $orderTransformer)
    {
        $orders = Order::all();
        //
        $fractal->setSerializer(new CustomSerializer());
        $collection = new Collection($orders, $orderTransformer);
        $data = $fractal->createData($collection)->toArray();
        return $this->respond($data);
    }

    /**
     * Create new order.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, Manager $fractal, OrderTransformer $orderTransformer)
    {
        $validator = Validator::make($request->all(), $this->validationOrder);
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return $this->respondWithError($errors);
        }
        //
        $pizza_ids = array_unique(explode(',',$request->get('pizzas','')));
        if (is_null($pizza_ids) || empty($pizza_ids)) {
            return $this->respondWithError('You have to select at least one pizza');
        }
        $total = 0;
        foreach ($pizza_ids as $pizza_id) {
            $pizza = Pizza::wherePizza_id($pizza_id)->first();
            if ($pizza != null) {
                $total = $total + $pizza->price;
            } else {
                unset($pizza_ids[array_search($pizza_id, $pizza_ids)]);
            }
        }
        if ($total == 0) {
            return $this->respondWithError('You have to select at least one pizza');
        }
        $data = $request->all();
        $data['total'] = $total;
        $order = new Order($data);
        if ($order->save()) {
            $order->pizzas()->attach($pizza_ids);
            $fractal->setSerializer(new CustomSerializer());
            $collection = new Item($order, $orderTransformer);
            $data = $fractal->createData($collection)->toArray();
            return response()->json($data);
        }
        return $this->respondWithError('Failed to create new order');
    }

    /**
     * Get order by id.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($order_id, Manager $fractal, OrderTransformer $orderTransformer)
    {
        $order = Order::whereOrder_id($order_id)->first();
        if ($order != null) {
            $fractal->setSerializer(new CustomSerializer());
            $collection = new Item($order, $orderTransformer);
            $data = $fractal->createData($collection)->toArray();
            return response()->json($data);
        }
        return $this->respondWithError('Order not found');
    }

    /**
     * Processing order
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function processing($order_id, Manager $fractal, OrderTransformer $orderTransformer)
    {
        $order = Order::whereOrder_id($order_id)->whereStatus(0)->first();
        if ($order != null) {
            $order->status = 1;
            $order->update();
            $fractal->setSerializer(new CustomSerializer());
            $collection = new Item($order, $orderTransformer);
            $data = $fractal->createData($collection)->toArray();
            return response()->json($data);
        }
        return $this->respondWithError('Order not found');
    }

    /**
     * Delivering order
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function delivering($order_id, Manager $fractal, OrderTransformer $orderTransformer)
    {
        $order = Order::whereOrder_id($order_id)->whereStatus(1)->first();
        if ($order != null) {
            $order->status = 2;
            $order->update();
            $fractal->setSerializer(new CustomSerializer());
            $collection = new Item($order, $orderTransformer);
            $data = $fractal->createData($collection)->toArray();
            return response()->json($data);
        }
        return $this->respondWithError('Order not found');
    }

    /**
     * Delivered order
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function delivered($order_id, Manager $fractal, OrderTransformer $orderTransformer)
    {
        $order = Order::whereOrder_id($order_id)->whereStatus(2)->first();
        if ($order != null) {
            $order->status = 3;
            $order->update();
            $fractal->setSerializer(new CustomSerializer());
            $collection = new Item($order, $orderTransformer);
            $data = $fractal->createData($collection)->toArray();
            return response()->json($data);
        }
        return $this->respondWithError('Order not found');
    }

}
