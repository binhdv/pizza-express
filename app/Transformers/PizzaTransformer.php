<?php

namespace App\Transformers;

use App\Models\Pizza;
use League\Fractal\TransformerAbstract;

class PizzaTransformer extends TransformerAbstract {

    public function transform(Pizza $pizza)
    {

        return [
            'id'                => $pizza->pizza_id,
            'name'              => $pizza->name,
            'description'       => $pizza->description,
            'type'              => $pizza->type,
            'price'             => doubleval($pizza->price),

        ];
    }


}
