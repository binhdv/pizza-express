<?php

namespace App\Transformers;

use App\Enum\OrderStatus;
use App\Models\Order;
use League\Fractal\TransformerAbstract;

class OrderTransformer extends TransformerAbstract {

    protected $defaultIncludes = ['pizzas'];

    public function transform(Order $order)
    {

        return [
            'id'                        => $order->order_id,
            'customer_name'             => $order->customer_name,
            'customer_address'          => $order->customer_address,
            'customer_phone'            => $order->customer_phone,
            'customer_description'      => $order->customer_description,
            'total'                     => doubleval($order->total),
            'status'                    => OrderStatus::getStatus($order->status),

        ];
    }

    public function includePizzas(Order $order) {
        $pizzas = $order->pizzas;
        if(!is_null($pizzas))
            return $this->collection($pizzas,new PizzaTransformer(),false);
    }
}
